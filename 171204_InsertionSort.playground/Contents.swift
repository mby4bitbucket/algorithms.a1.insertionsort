//: Playground - noun: a place where people can play

func insertionSort(_ arr: [Int]) -> [Int] {
    var res = arr
    
    for i in 1..<arr.count {
        let key = res[i]
        
        var j = i
        while (j > 0) && (key < res[j - 1]) {
            res[j] = res[j - 1]
            j = j - 1
        }
        res[j] = key
    }
    
    return res
}

insertionSort([4,2,1])
